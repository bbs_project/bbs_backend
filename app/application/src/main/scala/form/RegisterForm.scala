package form

import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._

import scala.util.matching.Regex

case class RegisterForm(username: String, password: String, email: String) {
  require(username.nonEmpty,Json.obj("username" -> "Username must not be empty"))
  require(username.length <= 20, Json.obj("username" -> "Username must be less than 20 characters"))
  require(email.nonEmpty, Json.obj("email" ->"Mail address is required"))
  require(email.matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$"), Json.obj("email"->"Mail address must has format xxx@yyy.zzz"))
  require(password.nonEmpty, Json.obj("password" -> "Password is required"))
  require(password.matches("""^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$"""), Json.obj("password" -> "Password must have at least 8 characters including letters, numbers and special characters"))
}

object RegisterForm {
  implicit val registerForm = Json.format[RegisterForm]
}