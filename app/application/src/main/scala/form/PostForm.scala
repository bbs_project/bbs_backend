package form


import play.api.libs.json.{Json}

case class PostForm(title: String,
                    content: String,
                    thumbnail: String
                   ) {
  require(title.length > 0, Json.obj("title" -> "Title must not be empty"))
  require(title.length < 150, Json.obj("title" -> "Title must not less than 150 characters"))
  require(content.length > 200, Json.obj("content" -> "Content must be longer than 200 words"))
  require(thumbnail.length > 0, Json.obj("thumbnail" -> "Thumbnail must not be empty"))
}

object PostForm {
  implicit val postForm = Json.format[PostForm]
}