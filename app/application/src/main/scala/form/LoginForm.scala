package form

import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._

case class LoginForm(email: String, password: String) {
  require(email.nonEmpty, Json.obj("email" -> "Mail address is required"))
  require(email.matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$"), Json.obj("email" -> "Mail address must has format xxx@yyy.zzz"))
  require(password.nonEmpty, Json.obj("password" -> "Password must not be empty"))
  require(password.length >= 8, Json.obj("password" -> "Password must be at least 8 characters"))
  require(password.matches("""^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$"""), Json.obj("password" ->"Password must have at least 8 characters including letters, numbers and special characters"))
}

object LoginForm {
  implicit val loginForm = Json.format[LoginForm]
}
