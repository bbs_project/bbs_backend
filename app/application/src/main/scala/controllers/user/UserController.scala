package controllers.user

import auth.Authenticator
import org.mindrot.jbcrypt.BCrypt
import form.{LoginForm, RegisterForm}
import internal.auth.AuthService
import internal.user.{User, UserId, UserService}
import play.api.libs.json._
import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents}


import javax.inject._
import scala.util.{Failure, Success}


@Singleton
class UserController @Inject()(val controllerComponents: ControllerComponents,
                               userService: UserService,
                               authService: AuthService,
                               authenticator: Authenticator) extends BaseController {

  def getAllUser(): Action[AnyContent] = Action { implicit request =>
    userService.getAllUser() match {
      case Success(value) => Ok(Json.toJson(value))
      case Failure(exception) => BadRequest("Error: " + exception)
    }
  }

  def signUp(): Action[AnyContent] = Action { implicit request =>
    try {
      val data = request.body.asJson.get.as[RegisterForm]
      val user = User(UserId(""), data.username, BCrypt.hashpw(data.password, BCrypt.gensalt()), data.email)
      if (authService.signUpValidate(user.email, user.username)) {
        userService.createUser(user) match {
          case Success(_) => Ok("User created successfully")
          case Failure(_) => Conflict(Json.obj("invalid" -> "Username or email already exists"))
        }
      }
      else {
        Conflict(Json.obj("invalid" -> "Username or email already exists"))
      }
    }
    catch {
      case e: IllegalArgumentException => Conflict(e.getMessage.replaceFirst("requirement failed: ",""))
    }
  }


  def login(): Action[AnyContent] = Action { implicit request =>
    try {
      val data = request.body.asJson.get.as[LoginForm]
      val loginData = User(UserId(""), "", data.password, data.email)
      authService.verify(loginData) match {
        case Success(value) =>
          println("Demo")
          println(value)
          Ok(Json.obj("status" -> "success", "data" -> value.username)).withSession("id" -> value.id.value,
            "email" -> value.email,
            "username" -> value.username
          )
        case Failure(_) => Conflict(Json.obj("invalid" -> "Invalid username or password"))
      }
    } catch {
      case e: IllegalArgumentException => Conflict(e.getMessage.replaceFirst("requirement failed: ",""))
    }
  }

  def logout(): Action[AnyContent] = authenticator { implicit request =>
    Ok.withSession()
  }

  def refresh(): Action[AnyContent] = authenticator { implicit request =>
    Ok(Json.obj("status" -> "success", "username" -> request.user.username))
  }
}