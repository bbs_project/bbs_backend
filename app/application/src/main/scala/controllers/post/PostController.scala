package controllers.post


import auth.Authenticator
import form.PostForm
import internal.post.{Post, PostId, PostService}
import org.joda.time.DateTime
import play.api.libs.json._
import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents}

import javax.inject._
import scala.util.{Failure, Success}


@Singleton
class PostController @Inject()(val controllerComponents: ControllerComponents,
                               postService: PostService,
                               authenticator: Authenticator) extends BaseController {

  def getAllPost(page: Int): Action[AnyContent] = Action {
    postService.getAllPost(page) match {
      case Success(value) => {
        postService.count() match {
          case Success(count) => Ok(Json.obj("count" -> count, "data" -> value))
          case Failure(exception) => BadRequest(exception.getMessage)
        }
      }
      case Failure(exception) => BadRequest(exception.getMessage)
    }
  }

  def getPostById(id: String): Action[AnyContent] = Action {
    postService.resolveById(id) match {
      case Success(value) => Ok(Json.toJson(value))
      case Failure(exception) => BadRequest(exception.getMessage)
    }
  }

  def create(): Action[AnyContent] = authenticator { implicit request =>
    try {
      val data = request.body.asJson.get.as[PostForm]
      val post = Post(PostId(""), data.title, data.content, data.thumbnail, request.user.username, DateTime.now(), DateTime.now())
      postService.createPost(post) match {
        case Success(_) => Ok(Json.obj("status"-> "success", "message" -> "Post created successfully"))
        case Failure(exception) => Conflict(Json.obj("Error"-> s"Failed to create post! Reason: ${exception.toString}"))
      }
    } catch {
      case e: IllegalArgumentException => Conflict(e.getMessage.replaceFirst("requirement failed: ", ""))
    }
  }
}