package auth

import internal.user.{User, UserService}

import javax.inject.Inject
import play.api.libs.json.Json
import play.api.mvc._
import play.api.mvc.Results.Forbidden

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}


case class UserRequest[A](user: User, request: Request[A]) extends WrappedRequest[A](request)

class Authenticator @Inject()(bodyParser: BodyParsers.Default, userService: UserService)(implicit ec: ExecutionContext)
  extends ActionBuilder[UserRequest, AnyContent] {

  override def parser: BodyParser[AnyContent] = bodyParser
  override protected def executionContext: ExecutionContext = ec
  override def invokeBlock[A](request: Request[A], block: UserRequest[A] => Future[Result]): Future[Result] = {
    request.session.get("id") match {
      case None => Future.successful(Forbidden(Json.obj("error" ->"No credential")))
      case Some(value) =>
        userService.findById(value) match {
          case Success(user) =>
            block(UserRequest(user, request))
          case Failure(exception) => Future.successful(Forbidden("Wrong credential" + exception))
        }
      }
    }
  }