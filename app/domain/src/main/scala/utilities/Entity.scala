package utilities

trait Entity[ID <: Identifier[_]] extends RecordTimestamp {
  val id: ID
}
