package internal.auth

import internal.user.{User, UserService}
import org.mindrot.jbcrypt.BCrypt

import javax.inject.{Inject, Singleton}
import scala.util._

@Singleton
class AuthService @Inject()(userService: UserService) {
  def verify(user: User): Try[User] = {
    val userFromDB = userService.findByEmail(user)
    if (BCrypt.checkpw(user.password, userFromDB.password))
      Success(userFromDB)
    else
      Failure(new Exception)
  }

  def signUpValidate(email: String, username: String) = {
    userService.signUpValidate(email, username)
  }
}
