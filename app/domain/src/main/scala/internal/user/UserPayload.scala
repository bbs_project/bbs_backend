package internal.user
import pdi.jwt._
import play.api.libs.json.Json.toJson
import play.api.libs.json.{Json, OFormat}

case class UserPayload(id: String, email: String, username: String)

object UserPayload {
  implicit val format: OFormat[UserPayload] = Json.format[UserPayload]
}