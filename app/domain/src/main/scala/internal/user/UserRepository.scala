package internal.user

import scala.util.Try

trait UserRepository {
  def resolveAllUser(): Try[List[User]]

  def create(user: User): Try[UserId]

  def findUser(user: User): User

  def findById(id: String): Try[User]

  def signUpValidate(email: String, username: String): Boolean

}
