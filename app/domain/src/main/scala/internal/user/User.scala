package internal.user

import play.api.libs.json.{JsValue, Json, Writes}


case class User(id: UserId, username: String, password: String, email: String)

object User {
  implicit val implicitUser = new Writes[User] {
    def writes(user: User): JsValue = {
      Json.obj(
        "id" -> user.id.value,
          "username" -> user.username,
          "email" -> user.email,
          "password" -> user.password
      )
    }
  }
}


