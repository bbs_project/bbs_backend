package internal.user

import javax.inject.Inject
import scala.util.Try

class UserService @Inject()(userRepository: UserRepository){
  def getAllUser(): Try[List[User]] =
    userRepository.resolveAllUser()

  def createUser(user: User):Try[UserId] = userRepository.create(user)

  def findByEmail(user: User): User = userRepository.findUser(user)

  def findById(id: String) : Try[User] = userRepository.findById(id: String)

  def signUpValidate(email: String, username: String) = userRepository.signUpValidate(email, username)

}
