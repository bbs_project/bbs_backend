package internal.user

import play.api.libs.functional.syntax.unlift
import play.api.libs.json._
import utilities.Identifier

case class UserId(value: String)

object UserId {
  implicit val implicitUserId = Json.format[UserId]
}