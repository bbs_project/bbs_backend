package internal.post

import play.api.libs.json.Json
import utilities.Identifier

import java.util.UUID



case class PostId(value: String)

object PostId {
  implicit val implicitPostId = Json.format[PostId]
  val id = UUID.randomUUID()
}

