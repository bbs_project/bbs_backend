package internal.post

import scala.util.Try

trait PostRepository {
    def resolveAllPost(page: Int): Try[List[Post]]
    def create(post: Post): Try[PostId]
    def resolveById(id : String): Try[Post]
    def count(): Try[Int]
}
