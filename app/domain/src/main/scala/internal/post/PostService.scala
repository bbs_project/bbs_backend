package internal.post

import javax.inject.{Inject}
import scala.util.Try


class PostService @Inject()(postRepository: PostRepository){
  def getAllPost(page: Int): Try[List[Post]] = postRepository.resolveAllPost(page)

  def createPost(post: Post): Try[PostId] = postRepository.create(post)

  def resolveById(id: String): Try[Post] = postRepository.resolveById(id)

  def count():Try[Int] = postRepository.count()
}
