package internal.post
import play.api.libs.json.JodaWrites._
import play.api.libs.json.JodaReads._
import internal.user.{User, UserId}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import play.api.libs.json.{JsValue, Json, Writes}
import java.time.format.DateTimeFormatter

case class Post(id: PostId,
                title: String,
                content: String,
                thumbnail: String,
                author_name: String,
                created_on:DateTime,
                updated_on: DateTime
               )

object Post {

  import org.joda.time.format.DateTimeFormat
  import org.joda.time.format.DateTimeFormatter

  val dtfOut: DateTimeFormatter = DateTimeFormat.forPattern("dd-MM-yyyy")
  implicit val implicitPost = new Writes[Post] {
    def writes(post: Post): JsValue = {
      Json.obj(
        "id" -> post.id.value,
        "author_name"-> post.author_name,
        "title" -> post.title,
        "content" -> post.content,
        "thumbnail" -> post.thumbnail,
        "created_on" -> dtfOut.print(post.created_on),
        "updated_on" -> dtfOut.print(post.updated_on)
      )
    }
  }
}
