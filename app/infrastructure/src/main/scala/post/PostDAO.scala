package post
import internal.post.{Post, PostId}
import internal.user.{User, UserId}
import scalikejdbc.WrappedResultSet
import skinny.orm.{Alias, SkinnyCRUDMapper, SkinnyCRUDMapperWithId, SkinnyNoIdCRUDMapper}
import user.UserDAO

object PostDAO extends SkinnyNoIdCRUDMapper[Post]{

  override lazy val tableName = "post"

  override lazy val defaultAlias = createAlias("p")
//
//  override def idToRawValue(id: PostId): Any = id.value
//
//  override def rawValueToId(value: Any) = PostId(value.toString)

  override def extract(rs: WrappedResultSet, n: scalikejdbc.ResultName[Post]): Post = new Post (
    id       = PostId(rs.get(n.id.value)),
    title = rs.get(n.title),
    content = rs.get(n.content),
    thumbnail = rs.get(n.thumbnail),
    author_name = rs.get(n.author_name),
    created_on = rs.get(n.created_on),
    updated_on = rs.get(n.updated_on)
  )
}
