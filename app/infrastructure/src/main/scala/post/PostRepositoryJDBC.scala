package post

import internal.post.{Post, PostId, PostRepository}
import scalikejdbc.sqls
import skinny.Pagination
import java.util.UUID
import javax.inject.Singleton
import scala.util.Try

@Singleton
class PostRepositoryJDBC extends PostRepository {
  override def resolveAllPost(page: Int): Try[List[Post]] = Try {
    val p = PostDAO.defaultAlias

    PostDAO.findAllByWithPagination(sqls.isNotNull(p.id).orderBy(p.created_on).desc, Pagination.page(page).per(10))
  }
  override def create(post: Post): Try[PostId] =Try {
    println(post)
    val id = UUID.randomUUID().toString
    PostDAO.createWithAttributes(
      Symbol("id") -> id,
      Symbol("title") -> post.title,
      Symbol("content") -> post.content,
      Symbol("thumbnail") -> post.thumbnail,
      Symbol("author_name") -> post.author_name,
      Symbol("created_on") -> post.created_on,
      Symbol("updated_on") -> post.updated_on
    )
    PostId(post.id.value)
  }

  override def resolveById(id: String): Try[Post] = Try {
    PostDAO.findBy(sqls.eq(PostDAO.defaultAlias.id, id)).get
  }


  override def count(): Try[Int] = Try {
    math.ceil(PostDAO.count() / 10.0).toInt
  }

}
