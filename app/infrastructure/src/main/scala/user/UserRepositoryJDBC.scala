package user

import internal.user.{User, UserId, UserPayload, UserRepository}
import scalikejdbc._

import java.util.UUID
import javax.inject.Singleton
import scala.util.Try

@Singleton
class UserRepositoryJDBC extends UserRepository {
  val u = UserDAO.defaultAlias
  override def resolveAllUser(): Try[List[User]] = Try {
    UserDAO.findAll()
  }

  override def create(user: User): Try[UserId] = Try {
    val id = UUID.randomUUID().toString
    UserDAO.createWithAttributes(
      Symbol("id") -> id,
      Symbol("username") -> user.username,
      Symbol("password") -> user.password,
      Symbol("email") -> user.email
    )
    UserId(id)
  }

  override def findUser(user: User): User = {
    UserDAO.findBy(sqls.eq(u.email, user.email)).get
  }


  override def findById(id: String): Try[User] = Try {
    UserDAO.findBy(sqls.eq(u.id, id)).get

  }

  override def signUpValidate(email: String, username: String): Boolean = {
    UserDAO.findBy(sqls.eq(u.email, email)).isDefined || UserDAO.findBy(sqls.eq(u.username, username)).isDefined
  }


}
