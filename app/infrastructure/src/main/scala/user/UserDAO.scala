package user

import internal.user.{User, UserId}

import scalikejdbc.WrappedResultSet
import skinny.orm._

object UserDAO extends SkinnyNoIdCRUDMapper[User]{
  override lazy val tableName = "user"
  override lazy val defaultAlias: Alias[User] = createAlias("u")

  override def extract(rs: WrappedResultSet, n: scalikejdbc.ResultName[User]): User = new User (
    id       = UserId(rs.get(n.id.value)),
    username = rs.get(n.username),
    password = rs.get(n.password),
    email = rs.get(n.email)
  )
}
