#!/bin/sh
#ENV
# export DB_HOST=my-db.chxzhlft28vc.ap-east-1.rds.amazonaws.com
# export DB_PORT=3306
# export DB_NAME=bbsbackend
# export DB_USER=root
# export DB_PASSWORD=nghia270901
# export FRONTEND_HOST=43.198.183.131
# export SECRET='QCY?tAnfk?aZ?iwrNwnxIlR6CTf:G3gf:90Latabg@5241AB`R5W:1uDFN];Ik@n'
DB_HOST=$DB_HOST
DB_PORT=$DB_PORT
DB_NAME=$DB_NAME
DB_USER=$DB_USER
DB_PASSWORD=$DB_PASSWORD
FRONTEND_HOST=$FRONTEND_HOST
SECRET=$SECRET

# rm -rf prod.conf
# rm -rf ./target

raw_config_file_path="./src/main/resources/raw_application.conf"
main_config_file_path="./dev.conf"
# Check if the application.conf.template file exists
if [ ! -f "${raw_config_file_path}" ]; then
  echo "ERROR: raw_application.conf file not found."
  exit 1
fi

# Check if required environment variables are set
if [ -z "${DB_HOST}" ] || [ -z "${DB_PORT}" ] || [ -z "${DB_NAME}" ] || [ -z "${DB_USER}" ] || [ -z "${DB_PASSWORD}" ]; then
  echo "ERROR: Required environment variables are not set."
  exit 1
fi

# Replace environment variables in the application.conf file
sed -e "s|\${DB_HOST}|${DB_HOST}|g" \
    -e "s|\${DB_PORT}|${DB_PORT}|g" \
    -e "s|\${DB_NAME}|${DB_NAME}|g" \
    -e "s|\${DB_USER}|${DB_USER}|g" \
    -e "s|\${DB_PASSWORD}|${DB_PASSWORD}|g" \
    -e "s|\${FRONTEND_HOST}|${FRONTEND_HOST}|g" \
    $raw_config_file_path > $main_config_file_path

echo "play.http.secret.key=\"$SECRET\"" >> $main_config_file_path

echo "Environment variables replaced successfully in application.conf."

# exec sbt dist
