import Dependencies._

ThisBuild / version := "1.0-SNAPSHOT"
name := """bbs_backend_nghia_nt"""
organization := "vn.flinters"

lazy val commonSettings = Seq(
  scalaVersion := "2.13.8",
  libraryDependencies ++= defaultDependencies,
  libraryDependencies ++= addedDependencies,
  resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases",
  resolvers += "Akka Snapshot Repository" at "https://repo.akka.io/snapshots/",
  Compile / scalaSource := baseDirectory.value / "src" / "main" / "scala",
  Compile / resourceDirectory := baseDirectory.value / "src" / "main" / "resources",
)



lazy val root = (project in file("."))
  .enablePlugins(PlayScala, SbtWeb)
  .disablePlugins(PlayLayoutPlugin)
  .aggregate(application, domain, infrastructure)
  .dependsOn(application, domain, infrastructure)
  .settings(
    commonSettings,
    Global / onChangedBuildSource := ReloadOnSourceChanges,
    PlayKeys.fileWatchService := play.dev.filewatch.FileWatchService.jdk7(play.sbt.run.toLoggerProxy(sLog.value)),
    PlayKeys.playDefaultPort := 9000
  )

lazy val application = (project in file("app/application"))
  .enablePlugins(PlayScala, SbtWeb)
  .dependsOn(infrastructure, domain)
  .settings(
    commonSettings,
  )

lazy val domain = (project in file("app/domain"))
  .enablePlugins(PlayScala)
  .settings(commonSettings)

lazy val infrastructure = (project in file("app/infrastructure"))
  .settings(commonSettings)
  .enablePlugins(PlayScala)
  .dependsOn(domain)
  .settings(libraryDependencies += jwtDependency)