import sbt._
import play.sbt.PlayImport._

object Dependencies {
    lazy val defaultDependencies = Seq(jdbc, ehcache, ws, specs2 % Test, guice, evolutions)

    lazy val addedDependencies = Seq(
      "mysql" % "mysql-connector-java" % "8.0.29", // your jdbc driver here
      "org.scalikejdbc" %% "scalikejdbc" % "3.5.0",
      "org.scalikejdbc" %% "scalikejdbc-config" % "3.5.0",
      "org.scalikejdbc" %% "scalikejdbc-play-initializer" % "2.8.0-scalikejdbc-3.5",
      "org.skinny-framework" %% "skinny-framework" % "3.1.0",
      "joda-time" % "joda-time" % "2.10.13",
      "com.typesafe.play" %% "play-json-joda" % "2.9.2",
      "org.scalikejdbc" %% "scalikejdbc-play-dbapi-adapter" % "2.8.0-scalikejdbc-3.5",
      "com.github.t3hnar" %% "scala-bcrypt" % "4.3.0",
      "com.pauldijou" %% "jwt-play" % "4.0.0",
      "com.pauldijou" %% "jwt-core" % "4.0.0",
      "com.auth0" % "jwks-rsa" % "0.6.1"

    )

    lazy val bcryptDependency = "org.mindrot" % "jbcrypt" % "0.4"

    lazy val jwtDependency = "com.github.jwt-scala" %% "jwt-core" % "9.0.3"

}
