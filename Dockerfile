FROM hseeberger/scala-sbt:11.0.14.1_1.6.2_2.12.15

WORKDIR /app

COPY ./target/universal/bbs_backend_nghia_nt-1.0-SNAPSHOT.zip .

COPY ./dev.conf .

RUN unzip bbs_backend_nghia_nt-1.0-SNAPSHOT.zip

WORKDIR /app/bbs_backend_nghia_nt-1.0-SNAPSHOT

EXPOSE 9000

CMD ["./bin/bbs_backend_nghia_nt", "-Dconfig.file=../dev.conf"]
