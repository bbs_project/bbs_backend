-- !Ups
create table post
(
    id         varchar(255) not null,
    title      varchar(255) not null,
    content    text not null,
    thumbnail  text not null,
    author_name varchar(255) not null,
    created_on datetime     not null,
    updated_on datetime     not null,
    constraint post_pk
        primary key (id)
);
-- !Downs
DROP TABLE post;