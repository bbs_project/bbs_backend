-- !Ups
create table user
(
    id       varchar(255) not null,
    username varchar(255) not null unique,
    password varchar(255) not null,
    email    varchar(255) not null unique,
    constraint user_pk
        primary key (id)

);
-- !Downs
DROP TABLE user;