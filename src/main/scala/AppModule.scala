import com.google.inject.AbstractModule
import internal.post.PostRepository
import internal.user.UserRepository
import post.PostRepositoryJDBC
import user.UserRepositoryJDBC

class AppModule extends AbstractModule {

  lazy val postRepository: PostRepository = new PostRepositoryJDBC
  lazy val userRepository: UserRepository = new UserRepositoryJDBC


  override def configure(): Unit = {
    bind(classOf[PostRepository]).toInstance(postRepository)
    bind(classOf[UserRepository]).toInstance(userRepository)
  }
}
